
ndevices = 5;

lastcnt = uint8(zeros([ndevices 1]));
firstcnt = true([ndevices 1]);
missed = uint32(zeros([ndevices 1]));
succeded = uint32(zeros([ndevices 1]));
disp('Counting:');
for idx = 1:length(out_channel)
	addr = uint32(out_data(idx,2)) ...
		+ uint32(out_data(idx,3))*2^8 ...
		+ uint32(out_data(idx,4))*2^16 ...
		+ uint32(out_data(idx,5))*2^24;	%Get device address

	if(addr == hex2dec('20500c30'))	%Which device is this
		type = 1;
	elseif(addr == hex2dec('204a0c29'))
		type = 2;
	else
		type = ndevices;
		fprintf('Error at time=%16.16f\n', out_time(idx));
	end
	
	if(firstcnt(type))
		firstcnt(type) = false;	%Counter initialization
		diff = 0;
	else
		diff = int32(out_data(idx,7)) - int32(lastcnt(type)) - 1;	%Difference between now and last counter
		if(diff < 0)
			diff = diff + 256;
		end
	end
	
	if(diff)	%Some packet was missed
		if(type == 1)	%Print missed
			if(diff == 255)
				fprintf('Copy at channel=%i\n', out_channel(idx));
			end
			fprintf('m%i', diff);
		end
		missed(type) = missed(type) + uint32(diff);	%Count missed
	end
	
	succeded(type) = succeded(type) + 1;	%Count successfully received
	if(type == 1)	%Print success
		fprintf('.');
		if(mod(succeded(1), 16) == 0)
			fprintf('\n');
		end
	end
	
	lastcnt(type) = int32(out_data(idx,7));	%Update counter
end

fprintf('\nEnd:\n');
for type = 1:ndevices
	 fprintf('  Type%i: Success=%i, Missed=%i, %%=%f\n', type, succeded(type), missed(type), 100*double(missed(type))/double((succeded(type)+double(missed(type)))));
end