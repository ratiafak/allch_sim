%% Erase trash
clear all;

%% Data for simulation

dcrem_filt = 8;
enable_pream_filter = 4;
enable_pream_length = 16;
enable_pream_limit = 6;
corr_pream_filt = 4;
enable_rssi_filt = 8;
enable_rssi_limit = 1;
enable_rssi_multiple = 4;
enable_samples = 8;
enable_sync_limit = 15;
sync_corr_limit = 10;
syncword = de2bi(hex2dec('a1a581f2'), 32);
corr_syncword_mask = flip(reshape(repmat((syncword*2-1), [2 1]), [1 2*length(syncword)]));

fftsize = 64;
sampling = 2.4E+6;

