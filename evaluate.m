
%% Scan out_data
cnt_good = zeros([1 64]);
cnt_bad = zeros([1 64]);
cnt_wrchan = 0;
cnt_error = 0;
cnt_miss = 0;
mostmiss = int32(0);
cnt_repeat = 0;
startidx = 26;
%address = [38 12 93 32];    %L3
address = [48 12 109 32];   %L1

lastcnt = mod(int32(out_data(startidx,7)) + int32(out_data(startidx,8))*256, 2^16);    % Initial counter value
cnt_good(out_data(startidx, 6)+1) = 1;   % One good packet at this channel

for idx = (startidx+1):length(out_channel) % Start by the second packet
    if any(out_data(idx, [1:5 9:21]) ~= [20 address 8:20]) || (out_data(idx, 6) >= 64)
        cnt_error = cnt_error + 1;  % Add one to error packet
    else
        if out_data(idx, 6) ~= out_channel(idx)
            cnt_wrchan = cnt_wrchan+1;  % Add one to wrong channel
        end
        cnt = mod(int32(out_data(idx,7)) + int32(out_data(idx,8))*256, 2^16);    % Read counter value
        miss = mod(cnt - lastcnt - int32(1), 2^16);  % Get difference from last counter - 1, number of packets missed
        lastcnt = cnt;
        
        if miss == 2^16-1   % Repeated packet
            cnt_repeat = cnt_repeat+1;
        else
            cnt_miss = cnt_miss + miss;
            if miss > mostmiss   % Maximum missed
                mostmiss = miss;
            end

            for jdx = 1:miss   % Increment counters for all missed packets
                cnt_bad(mod(int32(out_data(idx, 6))-jdx, 64)+1)...
                    = cnt_bad(mod(int32(out_data(idx, 6))-jdx, 64)+1) + 1;  % Add missed packets to bad
            end
            cnt_good(out_data(idx, 6)+1) = cnt_good(out_data(idx, 6)+1) + 1;  % Add one to good
        end
    end
end

disp(['Wrong channel ' num2str(cnt_wrchan)]);
disp(['Repeated      ' num2str(cnt_repeat)]);
disp(['Wrong packet  ' num2str(cnt_error)]);
disp(['Max missed    ' num2str(mostmiss)]);
disp(['Received ' num2str(sum(cnt_good)) ' or ' num2str(100*sum(cnt_good)/(sum(cnt_good)+sum(cnt_bad))) '%']);
disp(['Missed ' num2str(sum(cnt_bad)) ' or ' num2str(100*sum(cnt_bad)/(sum(cnt_good)+sum(cnt_bad))) '%']);

%% Store
save(['results_' datestr(datetime('now'))], 'cnt_wrchan', 'cnt_error', 'cnt_good', 'cnt_bad', 'mostmiss');

%% Plot the results
plot(0:63, cnt_good./(cnt_good+cnt_bad));

